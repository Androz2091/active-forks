const numberToText = (number, intDigits, floatDigits) => Intl.NumberFormat(
    'en-US',
    { minimumIntegerDigits: intDigits, minimumFractionDigits: floatDigits, maximumFractionDigits: floatDigits }
).format(number);

document.addEventListener('DOMContentLoaded', () => {
    halfmoonOnDOMContentLoaded();

    document.querySelector('#app__header__search__inputContainer__input').addEventListener('keydown', event => {
        if(event.key.endsWith('Enter'))
            document.querySelector('#app__header__search__button').click();
    });

    document.querySelector('#app__header__search__button').addEventListener('click', async () => {
        document.querySelector('#app__header__search').setAttribute('disabled', '');
        document.querySelector('#app__header__status__value').textContent = 'searching';
        document.querySelector('#app__main').style.display = 'none';
        document.querySelector('#app__main__resultsContainer__results').innerHTML = '';
        try {
            const
                token = localStorage.getItem('token'),
                repository = document.querySelector('#app__header__search__inputContainer__input').value,
                config = token ? {
                    headers: {
                        authorization: `token ${token}`
                    }
                } : {},
                {
                    html_url: repositoryLink,
                    default_branch: repositoryDefaultBranch,
                    stargazers_count: repositoryStarCount,
                    forks_count: repositoryForkCount,
                    size: repositorySize
                } = await (await fetch(`https://api.github.com/repos/${repository}`, config)).json(),
                [{
                    sha,
                    html_url: commitLink,
                    commit: {
                        author: {
                            name: commitAuthorName,
                            date: commitDate
                        },
                        message: commitMessage
                    },
                    author
                }] = await (await fetch(`https://api.github.com/repos/${repository}/commits?per_page=1`, config)).json(),
                {
                    login: commitAuthorLogin,
                    html_url: commitAuthorLink
                } = author || {},
                forks = [],
                results = [{
                    isOriginalRepo: true,
                    repository,
                    repositoryLink,
                    repositoryDefaultBranch,
                    repositoryStarCount,
                    repositoryForkCount,
                    repositorySize,
                    commitId: sha.slice(0, 7),
                    commitLink,
                    commitDate: new Date(commitDate).valueOf(),
                    commitAuthor: commitAuthorLogin || commitAuthorName,
                    commitAuthorLink,
                    commitMessage
                }];
            {
                let currentPage = 1, lastPageCount;
                do {
                    const _forks = await (await fetch(
                        `https://api.github.com/repos/${repository}/forks?per_page=100&page=${currentPage++}`,
                        config
                    )).json();
                    lastPageCount = _forks.length;
                    forks.push(..._forks);
                } while(lastPageCount);
            }
            let notFoundCount = 0;
            for(const [index, {
                full_name: repository,
                html_url: repositoryLink,
                default_branch: repositoryDefaultBranch,
                stargazers_count: repositoryStarCount,
                forks_count: repositoryForkCount,
                size: repositorySize
            }] of forks.entries()){
                const res = await fetch(`https://api.github.com/repos/${repository}/commits?per_page=1`, config);
                if(res.status === 200){
                    const
                        [{
                            sha,
                            html_url: commitLink,
                            commit: {
                                author: {
                                    name: commitAuthorName,
                                    date: commitDate
                                },
                                message: commitMessage
                            },
                            author
                        }] = await res.json(),
                        {
                            login: commitAuthorLogin,
                            html_url: commitAuthorLink
                        } = author || {};
                    results.push({
                        repository,
                        repositoryLink,
                        repositoryDefaultBranch,
                        repositoryStarCount,
                        repositoryForkCount,
                        repositorySize,
                        commitId: sha.slice(0, 7),
                        commitLink,
                        commitDate: new Date(commitDate).valueOf(),
                        commitAuthor: commitAuthorLogin || commitAuthorName,
                        commitAuthorLink,
                        commitMessage
                    });
                }
                else notFoundCount++;
                const
                    progressBar = document.querySelector('#app__header__progress__barContainer__bar'),
                    progressPercent = (index + 1) / forks.length * 100;
                progressBar.style.width = `${progressPercent}%`;
                progressBar.setAttribute('aria-valuenow', progressPercent.toString());
                document.querySelector('#app__header__progress__text').textContent = `${numberToText(index + 1 - notFoundCount, 3)}/${numberToText(forks.length - notFoundCount, 3)}`;
            }
            document.querySelector('#app__main').style.removeProperty('display');
            document.querySelector('#app__main__resultsContainer__results').innerHTML = results.sort((a, b) => b.commitDate - a.commitDate).map(item => `
                <tr>
                    <td><a href="${item.repositoryLink}" target="_blank">${item.repository}</a>${item.isOriginalRepo ? ' (original repo)' : ''}</td>
                    <td>${item.repositoryStarCount}</td>
                    <td>${item.repositoryForkCount}</td>
                    <td>${item.repositoryDefaultBranch}</td>
                    <td><a href="${item.commitLink}" target="_blank" title="${item.commitMessage}">${item.commitId}</a></td>
                    <td>${new Date(item.commitDate).toLocaleString()}</td>
                    <td>${item.commitAuthorLink ? `<a href="${item.commitAuthorLink}" target="_blank">${item.commitAuthor}</a>` : item.commitAuthor}</td>
                </tr>
            `).join('');
            document.querySelector('#app__header__search').removeAttribute('disabled');
            document.querySelector('#app__header__status__value').textContent = 'idle';
        } catch (e) {
            document.querySelector('#app__header__search').removeAttribute('disabled');
            document.querySelector('#app__header__status__value').textContent = `ratelimited ${!localStorage.getItem('token') ? '(try to use a token)' : ''}`;
        }
    });

    document.querySelector('#app__header__tokenInputContainer__input').value = localStorage.getItem('token') || null;

    document.querySelector('#app__header__tokenInputContainer__input').addEventListener('input', () =>
        localStorage.setItem('token', document.querySelector('#app__header__tokenInputContainer__input').value));
});